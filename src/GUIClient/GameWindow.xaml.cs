﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Checkers;
using Checkers.Events;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        private GameManager manager;
        private Ellipse selected;

        public GameWindow(bool forceJumps, bool vsAI)
        {
            InitializeComponent();

            btnCancelJump.IsEnabled = !forceJumps;

            manager = new GameManager(new GameBoard(), forceJumps, vsAI);
            manager.OnGameOver += OnGameOver;
            manager.Board.OnMoved += OnPieceMoved;
        }

        private void GameWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DrawBoard();
            DrawPieces();
            UpdatePlayerTurn();
        }

        #region Events
        private void OnPieceClicked(object sender, MouseEventArgs e)
        {
            Ellipse circle = sender as Ellipse;
            StackPanel panel = GetBoardCell(circle);

            if (panel is null)
                return;

            Piece piece = manager.Board[GetCellPosition(panel)];

            if (piece.Owner != manager.PlayerTurn)
                return;
            else if (!piece.AvailableMoves.Any(p => p != null))
                return;

            if (selected != null)
                selected.Stroke = null;

            circle.Stroke = new SolidColorBrush(GameColors.CheckerHighlightColor);
            circle.StrokeThickness = 5;
            selected = circle;
        }

        private void OnCellClicked(object sender, MouseEventArgs e)
        {
            if (selected is null)
                return;

            StackPanel panel = GetBoardCell(selected);

            if (!manager.Move(GetCellPosition(panel),
                              GetCellPosition(sender as StackPanel)))
                return;

            selected = null;
        }

        private async void OnPieceMoved(object sender, MoveEventArgs e)
        {
            string message = string.Empty;

            if (e.PieceMoved.Owner == Player.Player1)
                message = "Player 1: ";
            else
                message = "Player 2: ";

            message += e.OldPosition.BoardPositionString() + ' ' + e.NewPosition.BoardPositionString();

            Log(message);

            if (manager.CanJumpAgain && !manager.ForceJumps)
            {
                if (manager.VersusAI && manager.PlayerTurn == Player.Player1)
                    Log("Can jump again! Click \"Cancel Jump\" to cancel.");
                else if(!manager.VersusAI)
                    Log("Can jump again! Click \"Cancel Jump\" to cancel.");
            }

            UpdatePlayerTurn();
            await Application.Current.Dispatcher.InvokeAsync(new Action(DrawPieces));
            Board.UpdateLayout();
        }

        private void OnGameOver(object sender, GameOverEventArgs e)
        {
            string message = string.Empty;

            if (e.Winner == Player.Player1)
                message = "Player 1 (RED) Wins!";
            else if (e.Winner == Player.Player2)
                message = "Player 2 (BLACK) Wins!";
            else
                message = "Draw!";

            MessageBox.Show(message, "Game Over", 
                            MessageBoxButton.OK, MessageBoxImage.Information);

            Log(message);
        }
        #endregion

        #region Utils
        private StackPanel GetBoardCell(int row, int col)
        {
            for (int i = 0; i < Board.Children.Count; i++)
            {
                UIElement element = Board.Children[i];

                if (Grid.GetRow(element) == row && Grid.GetColumn(element) == col)
                    return element as StackPanel;
            }

            return null;
        }

        private StackPanel GetBoardCell(Ellipse circle)
        {
            if (circle is null)
                return null;

            Canvas canvas = circle.Parent as Canvas;
            return canvas.Parent as StackPanel;
        }

        private void EmptyCell(StackPanel panel)
        {
            Canvas canvas = panel.Children[0] as Canvas;
            canvas.Children.Clear();
        }

        private Checkers.Point GetCellPosition(UIElement element)
        {
            return new Checkers.Point(Grid.GetRow(element) + 1, Grid.GetColumn(element) + 1);
        }

        private void Log(string message)
        {
            boxGameFeed.Text += message + '\n';
            boxGameFeed.ScrollToEnd();
        }

        private void UpdatePlayerTurn()
        {
            switch(manager.PlayerTurn)
            {
                case Player.Player1:
                    txtblPlayerTurn.Foreground = new SolidColorBrush(GameColors.RedCheckerColor);
                    txtblPlayerTurn.Text = "Player 1";
                    break;
                case Player.Player2:
                    txtblPlayerTurn.Foreground = new SolidColorBrush(GameColors.BlackCheckerColor);
                    txtblPlayerTurn.Text = "Player 2";
                    break;
                case Player.None:
                    txtblPlayerTurn.Foreground = new SolidColorBrush(Colors.Black);
                    txtblPlayerTurn.Text = "Game Finished";
                    break;
            }
        }
        #endregion

        #region Draw
        private void DrawPieces()
        {
            for (int row = 1; row < GameBoard.BoardSize + 1; row++)
            {
                for (int col = 1; col < GameBoard.BoardSize + 1; col++)
                {
                    Piece piece = manager.Board[row, col];
                    StackPanel panel = GetBoardCell(row - 1, col - 1);

                    if (piece is null)
                    {
                        EmptyCell(panel);
                        continue;
                    }
                    
                    Canvas canvas = panel.Children[0] as Canvas;
                    Color color = piece.Owner == Player.Player1 ? GameColors.RedCheckerColor : GameColors.BlackCheckerColor;
                    Canvas.SetLeft(canvas, 0);
                    
                    Ellipse circle = new Ellipse()
                    {
                        Width = 80,
                        Height = 80,
                        Fill = new SolidColorBrush(color)
                    };
                    circle.MouseUp += OnPieceClicked;

                    Canvas.SetLeft(circle, 8);
                    Canvas.SetTop(circle, 8);
                    canvas.Children.Add(circle);
                    
                    if (piece.IsKing)
                    {
                        TextBlock block = new TextBlock()
                        {
                            Text = "K",
                            FontSize = 30,
                            Foreground = new SolidColorBrush(Colors.White),
                            TextAlignment = TextAlignment.Center
                        };

                        Canvas.SetLeft(block, 40);
                        Canvas.SetTop(block, 26);
                        canvas.Children.Add(block);
                    }
                }
            }
        }

        private void DrawBoard()
        {
            for (int row = 0; row < GameBoard.BoardSize; row++)
            {
                for (int col = 0; col < GameBoard.BoardSize; col++)
                {
                    StackPanel stackPanel = new StackPanel();
                    stackPanel.MouseUp += OnCellClicked;

                    if (row % 2 == 1)
                    {
                        if (col % 2 == 0)
                            stackPanel.Background = new SolidColorBrush(GameColors.DarkBrown);
                        else
                            stackPanel.Background = new SolidColorBrush(GameColors.LightBrown);
                    }
                    else
                    {
                        if (col % 2 == 0)
                            stackPanel.Background = new SolidColorBrush(GameColors.LightBrown);
                        else
                            stackPanel.Background = new SolidColorBrush(GameColors.DarkBrown);
                    }

                    Grid.SetRow(stackPanel, row);
                    Grid.SetColumn(stackPanel, col);

                    Canvas canvas = new Canvas();

                    stackPanel.Children.Add(canvas);
                    Board.Children.Add(stackPanel);
                }
            }
        }
        #endregion

        #region Button Events
        private void btnCancelJump_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                manager.CanJumpAgain = false;

                if (selected != null)
                    selected.Stroke = null;

                Log("Canceled jump.");
                UpdatePlayerTurn();
            }
            catch(InvalidOperationException)
            {
                Log("Cannot cancel jump!");
            }
        }

        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            if (!manager.Undo())
            {
                Log("No moves to undo!");
                return;
            }

            DrawPieces();
            UpdatePlayerTurn();
            selected = null;
        }

        private void btnRedo_Click(object sender, RoutedEventArgs e)
        {
            if (!manager.Redo())
            {
                Log("No moves to redo!");
                return;
            }

            DrawPieces();
            UpdatePlayerTurn();
            selected = null;
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            if (manager.IsGameOver)
            {
                MessageBoxResult result = MessageBox.Show("Would you like to save your game for replaying before exiting?", "Exit",
                                          MessageBoxButton.YesNo, MessageBoxImage.Question);

                if(result == MessageBoxResult.Yes)
                {
                    TextInputPrompt prompt = new TextInputPrompt("Enter a name for the game:", "Game Name");
                    prompt.ShowDialog();

                    if (!prompt.WasCancelled)
                        SavedGamesManager.SaveGame(new SavedGame(prompt.Input, DateTime.Now,
                                                   manager.ExportMoves(), manager.PlayerTurn));
                    else
                        return;
                }

                this.Close();
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to exit?", "Exit",
                                                          MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                    this.Close();
            }
        }
        #endregion
    }
}
