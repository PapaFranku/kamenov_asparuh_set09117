﻿using System.Windows.Media;

namespace GUIClient
{
    public static class GameColors
    {
        public static readonly Color DarkBrown = (Color)ColorConverter.ConvertFromString("#AA6439");
        public static readonly Color LightBrown = (Color)ColorConverter.ConvertFromString("#FFCBAA");
        public static readonly Color RedCheckerColor = (Color)ColorConverter.ConvertFromString("#B40F0F");
        public static readonly Color BlackCheckerColor = Colors.Black;
        public static readonly Color CheckerHighlightColor = Colors.MediumAquamarine;
    }
}
