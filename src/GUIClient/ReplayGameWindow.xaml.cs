﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Checkers;
using Checkers.Events;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for ReplayGameWindow.xaml
    /// </summary>
    public partial class ReplayGameWindow : Window
    {
        private GameManager manager;
        private SavedGame savedGame;
        private List<string> log;
        private int currentMove;
        private bool isAuto;

        private const int REPLAY_SLOW = 1500;
        private const int REPLAY_MED = 1000;
        private const int REPLAY_FAST = 500;

        public ReplayGameWindow(SavedGame savedGame)
        {
            if (savedGame is null)
                throw new ArgumentNullException("Parameter " + nameof(savedGame) + " cannot be null!");

            InitializeComponent();

            log = new List<string>();

            manager = new GameManager(new GameBoard(), false, false);
            manager.OnGameOver += OnGameOver;

            this.savedGame = savedGame;
            currentMove = 0;

            ReplayWindow.Title = savedGame.Name;
        }

        private void ReplayWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DrawBoard();
            DrawPieces();
        }

        private void OnGameOver(object sender, GameOverEventArgs e)
        {
            string message = string.Empty;

            if (e.Winner == Player.Player1)
                message = "Player 1 (RED) Wins!";
            else if (e.Winner == Player.Player2)
                message = "Player 2 (BLACK) Wins!";
            else
                message = "Draw!";

            MessageBox.Show(message, "Game Over",
                            MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void NextMove()
        {
            if (currentMove != savedGame.Moves.Length)
            {
                string message = manager.PlayerTurn.ToString() + ": ";

                Move move = savedGame.Moves[currentMove];
                manager.Move(move.From, move.To);

                currentMove++;

                if (!btnPrev.IsEnabled)
                    btnPrev.IsEnabled = true;

                if (currentMove == savedGame.Moves.Length)
                    btnNext.IsEnabled = false;

                DrawPieces();
                UpdatePlayerTurn();

                message += move.From.BoardPositionString() + ' ' + move.To.BoardPositionString();
                Log(message);
            }
        }

        private void PreviousMove()
        {
            if (manager.Undo())
            {
                if (currentMove > 0)
                    currentMove--;

                if (!btnNext.IsEnabled)
                    btnNext.IsEnabled = true;

                if (currentMove == 0)
                    btnPrev.IsEnabled = false;

                DrawPieces();
                UpdatePlayerTurn();
                RemoveLastLog();
            }
        }

        #region Button Events
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (isAuto)
                return;

            NextMove();
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            if (isAuto)
                return;

            PreviousMove();
        }

        private async void btnAuto_Click(object sender, RoutedEventArgs e)
        {
            if (isAuto)
            {
                isAuto = false;
                ReplaySpeed.IsEnabled = true;
            }
            else
            {
                isAuto = true;
                ReplaySpeed.IsEnabled = false;

                btnAuto.Content = "Stop";
                await PlayAuto(GetReplaySpeed());
                btnAuto.Content = "Auto Replay";
            }
        }

        private async Task PlayAuto(int replaySpeed)
        {
            while (!manager.IsGameOver && isAuto)
            {
                await Task.Delay(replaySpeed);

                NextMove();
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to exit?", "Exit",
                                          MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
                this.Close();
        }
        #endregion

        #region Utils
        private void UpdatePlayerTurn()
        {
            switch (manager.PlayerTurn)
            {
                case Player.Player1:
                    txtblPlayerTurn.Foreground = new SolidColorBrush(GameColors.RedCheckerColor);
                    txtblPlayerTurn.Text = "Player 1";
                    break;
                case Player.Player2:
                    txtblPlayerTurn.Foreground = new SolidColorBrush(GameColors.BlackCheckerColor);
                    txtblPlayerTurn.Text = "Player 2";
                    break;
                case Player.None:
                    txtblPlayerTurn.Foreground = new SolidColorBrush(Colors.Black);
                    txtblPlayerTurn.Text = "Game Finished";
                    break;
            }
        }

        private int GetReplaySpeed()
        {
            RadioButton radio = ReplaySpeed.Children.OfType<RadioButton>()
                   .FirstOrDefault(r => r.IsChecked != null && r.IsChecked == true);

            if (radio is null)
                return REPLAY_MED;

            switch (radio.Content)
            {
                case "Slow":
                    return REPLAY_SLOW;
                case "Medium":
                    return REPLAY_MED;
                case "Fast":
                    return REPLAY_FAST;
                default:
                    return REPLAY_MED;
            }
        }

        private StackPanel GetBoardCell(int row, int col)
        {
            for (int i = 0; i < Board.Children.Count; i++)
            {
                UIElement element = Board.Children[i];

                if (Grid.GetRow(element) == row && Grid.GetColumn(element) == col)
                    return element as StackPanel;
            }

            return null;
        }

        private void EmptyCell(StackPanel panel)
        {
            Canvas canvas = panel.Children[0] as Canvas;
            canvas.Children.Clear();
        }

        private void Log(string message)
        {
            log.Add(message + '\n');
            boxGameFeed.Text = log.Aggregate((x, y) => x + y);
            boxGameFeed.ScrollToEnd();
        }

        private void RemoveLastLog()
        {
            if (log.Count == 0)
                return;

            log.RemoveAt(log.Count - 1);

            if (log.Count != 0)
                boxGameFeed.Text = log.Aggregate((x, y) => x + y);
            else
                boxGameFeed.Text = string.Empty;
        }
        #endregion    

        #region Draw
        private void DrawPieces()
        {
            for (int row = 1; row < GameBoard.BoardSize + 1; row++)
            {
                for (int col = 1; col < GameBoard.BoardSize + 1; col++)
                {
                    Piece piece = manager.Board[row, col];
                    StackPanel panel = GetBoardCell(row - 1, col - 1);

                    if (piece is null)
                    {
                        EmptyCell(panel);
                        continue;
                    }

                    Canvas canvas = panel.Children[0] as Canvas;
                    Color color = piece.Owner == Player.Player1 ? GameColors.RedCheckerColor : GameColors.BlackCheckerColor;
                    Canvas.SetLeft(canvas, 0);

                    Ellipse circle = new Ellipse()
                    {
                        Width = 80,
                        Height = 80,
                        Fill = new SolidColorBrush(color)
                    };

                    Canvas.SetLeft(circle, 8);
                    Canvas.SetTop(circle, 8);
                    canvas.Children.Add(circle);

                    if (piece.IsKing)
                    {
                        TextBlock block = new TextBlock()
                        {
                            Text = "K",
                            FontSize = 30,
                            Foreground = new SolidColorBrush(Colors.White),
                            TextAlignment = TextAlignment.Center
                        };

                        Canvas.SetLeft(block, 40);
                        Canvas.SetTop(block, 26);
                        canvas.Children.Add(block);
                    }
                }
            }
        }

        private void DrawBoard()
        {
            for (int row = 0; row < GameBoard.BoardSize; row++)
            {
                for (int col = 0; col < GameBoard.BoardSize; col++)
                {
                    StackPanel stackPanel = new StackPanel();

                    if (row % 2 == 1)
                    {
                        if (col % 2 == 0)
                            stackPanel.Background = new SolidColorBrush(GameColors.DarkBrown);
                        else
                            stackPanel.Background = new SolidColorBrush(GameColors.LightBrown);
                    }
                    else
                    {
                        if (col % 2 == 0)
                            stackPanel.Background = new SolidColorBrush(GameColors.LightBrown);
                        else
                            stackPanel.Background = new SolidColorBrush(GameColors.DarkBrown);
                    }

                    Grid.SetRow(stackPanel, row);
                    Grid.SetColumn(stackPanel, col);

                    Canvas canvas = new Canvas();

                    stackPanel.Children.Add(canvas);
                    Board.Children.Add(stackPanel);
                }
            }
        }
        #endregion
    }
}
