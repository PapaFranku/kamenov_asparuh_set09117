﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.Serialization.Formatters.Binary;

namespace GUIClient
{
    public static class SavedGamesManager
    {
        private static readonly string SavedGamesFolder = Path.Combine(Directory.GetCurrentDirectory(), "SavedGames");

        static SavedGamesManager()
        {
            if (!Directory.Exists(SavedGamesFolder))
                Directory.CreateDirectory(SavedGamesFolder);
        }
   
        public static void SaveGame(SavedGame game)
        {
            try
            {
                using (Stream stream = File.Create(Path.Combine(SavedGamesFolder, game.Name + ".bin")))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, game);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error saving game", 
                                MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void DeleteGame(SavedGame game)
        {
            string path = Path.Combine(SavedGamesFolder, game.Name + ".bin");

            if (!File.Exists(path))
                return;

            try
            {
                File.Delete(path);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error deleting game",
                                MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static async Task<IEnumerable<SavedGame>> LoadSavedGames()
        {
            try
            {
                IEnumerable<string> files = Directory.EnumerateFiles(SavedGamesFolder, "*.bin");

                if(files.Count() == 0)
                    return Enumerable.Empty<SavedGame>();

                List<SavedGame> games = new List<SavedGame>(files.Count());
                BinaryFormatter bin = new BinaryFormatter();

                foreach (string file in files)
                {
                    string filePath = Path.Combine(Directory.GetCurrentDirectory(), file);

                    using (Stream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read,
                                                          bufferSize: 4096, useAsync: true))
                    {
                        byte[] byteArr = new byte[stream.Length];
                        await stream.ReadAsync(byteArr, 0, (int)stream.Length);

                        games.Add((SavedGame)bin.Deserialize(new MemoryStream(byteArr)));
                    }
                }

                return games;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error loading saved games",
                                MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return Enumerable.Empty<SavedGame>();
        }
    }
}
