﻿using System.Windows;
using System.Windows.Controls;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for TextInputPrompt.xaml
    /// </summary>
    public partial class TextInputPrompt : Window
    {
        public bool WasCancelled => cancel;
        public string Input => boxInput.Text;

        private bool cancel;

        public TextInputPrompt(string question, string title)
        {
            InitializeComponent();

            lblQuestion.Content = question;
            PromptWindow.Title = title;
            cancel = true;
        }

        private void boxInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnSubmit.IsEnabled = !string.IsNullOrEmpty(boxInput.Text);
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            this.Close();
        }
    }
}
