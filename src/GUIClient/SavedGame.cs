﻿using System;
using Checkers;

namespace GUIClient
{
    [Serializable]
    public class SavedGame
    {
        public string Name { get; }
        public DateTime DateSaved { get; }
        public Move[] Moves { get; }
        public string Winner { get; }

        public SavedGame(string name, DateTime saved, Move[] moves, Player winner)
        {
            Name = name;
            DateSaved = saved;
            Moves = moves;
            
            switch(winner)
            {
                case Player.None:
                    Winner = "Tie";
                    break;
                case Player.Player1:
                    Winner = "Red";
                    break;
                case Player.Player2:
                    Winner = "Black";
                    break;
            }
        }
    }
}
