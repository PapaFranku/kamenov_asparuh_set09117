﻿using System.Windows;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            GameWindow window = new GameWindow(chkForceJumps.IsChecked ?? false,
                                               chkAI.IsChecked ?? false);
            window.Owner = this.Owner;
            this.Hide();
            window.ShowDialog();
            this.Show();
        }

        private void btnReplay_Click(object sender, RoutedEventArgs e)
        {
            ReplayManager window = new ReplayManager();

            window.Owner = this.Owner;
            this.Hide();
            window.ShowDialog();
            this.Show();
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
