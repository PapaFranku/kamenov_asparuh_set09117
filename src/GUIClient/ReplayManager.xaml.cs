﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for ReplayManager.xaml
    /// </summary>
    public partial class ReplayManager : Window
    {
        private ObservableCollection<SavedGame> savedGames;

        public ReplayManager()
        {
            InitializeComponent();
        }

        private void LaunchReplay()
        {
            if (dgSavedGames.SelectedItem is null)
                return;

            ReplayGameWindow window = new ReplayGameWindow(dgSavedGames.SelectedItem as SavedGame);

            window.Owner = this.Owner;
            this.Close();
            window.ShowDialog();
        }

        private async void ReplayManagerWindow_Loaded(object sender, RoutedEventArgs e)
        {
            savedGames = new ObservableCollection<SavedGame>(await SavedGamesManager.LoadSavedGames());
            dgSavedGames.ItemsSource = savedGames;
        }

        private void dgSavedGames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgSavedGames.SelectedItem is null)
            {
                btnReplay.IsEnabled = false;
                btnDelete.IsEnabled = false;
            }
            else
            {
                btnReplay.IsEnabled = true;
                btnDelete.IsEnabled = true;
            }
        }

        private void dgSavedGames_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            LaunchReplay();
        }

        private void btnReplay_Click(object sender, RoutedEventArgs e)
        {
            LaunchReplay();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dgSavedGames.SelectedItem is null)
                return;

            SavedGamesManager.DeleteGame(dgSavedGames.SelectedItem as SavedGame);
            savedGames.Remove(dgSavedGames.SelectedItem as SavedGame);

            if (dgSavedGames.Items.Count != 0)
                dgSavedGames.SelectedIndex = 0;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
