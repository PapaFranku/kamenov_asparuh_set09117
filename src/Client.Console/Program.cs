﻿using System;

namespace ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();

            while (!client.GameOver)
                client.NextMove();

            Console.ReadKey();
        }
    }
}
