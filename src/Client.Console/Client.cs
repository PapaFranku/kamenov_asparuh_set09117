﻿using System;
using System.Linq;
using Checkers;
using Checkers.Events;

namespace ConsoleClient
{
    public class Client
    {
        public bool GameOver { get; private set; }
        private GameManager manager;

        public Client()
        {
            Console.WriteLine("The pieces can be moved by sprecifying the location of the piece to be moved and the new position.");
            Console.WriteLine("E.g: \"a6 b5\" moves the piece on A6 to B5");
            Console.WriteLine("Type \"cancel\" if you don't want to make further jumps when available.");
            Console.WriteLine("This will only work if the game mode is set to not force jumps.");
            Console.WriteLine("Type \"undo\" to undo a move.");
            Console.WriteLine("Type \"redo\" to redo a move.");

            NewGame();
        }

        public void NewGame()
        {
            bool forceJumps = GetYesNoInput("Set game mode to \"force jumps\"?");
            bool versusAI = GetYesNoInput("Play versus AI?");

            manager = new GameManager(new GameBoard(), forceJumps, versusAI);

            manager.Board.OnMoved += Board_OnMoved;
            manager.OnGameOver += Manager_OnGameOver;

            GameOver = false;

            Console.WriteLine();
            Draw();
        }

        public void Draw()
        {
            Console.Write("\t  ");
            Console.WriteLine(string.Join(" ", GameBoard.Alphabet));

            for (int x = 1; x < GameBoard.BoardSize + 1; x++)
            {
                Console.Write('\t' + (x).ToString() + '|');

                for (int y = 1; y < GameBoard.BoardSize + 1; y++)
                {
                    Piece piece = manager.Board[x, y];

                    if (piece is null || !piece.IsInPlay)
                    {
                        Console.Write(" |");
                        continue;
                    }

                    if (piece.Owner == Player.Player1)
                        Console.ForegroundColor = ConsoleColor.White;
                    else
                        Console.ForegroundColor = ConsoleColor.Red;

                    if (piece.IsKing)
                        Console.Write("K");
                    else
                        Console.Write("O");

                    Console.ForegroundColor = ConsoleColor.Gray;

                    Console.Write("|");
                }

                Console.WriteLine();
            }

            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public void NextMove()
        {
            if (manager.PlayerTurn == Player.Player1)
                Console.Write("\nPlayer 1: ");
            else
                Console.Write("\nPlayer 2: ");

            string input = Console.ReadLine().ToUpper();

            if (input == "CANCEL")
            {
                try
                {
                    manager.CanJumpAgain = false;
                }
                catch(InvalidOperationException)
                {
                    Console.WriteLine("Cannot cancel jump!");
                }

                return;
            }
            else if(input == "UNDO")
            {
                if (!manager.Undo())
                    Console.WriteLine("No moves to undo!");
                else
                    Draw();

                return;
            }
            else if(input == "REDO")
            {
                if (!manager.Redo())
                    Console.WriteLine("No moves to redo!");
                else
                    Draw();

                return;
            }

            if (!VerifyInput(input, out string[] commands))
            {
                Console.WriteLine("Invalid input!");
                return;
            }

            Point from = new Point(int.Parse(commands[0][1].ToString()), GetIndexForLetter(commands[0][0]));
            Point to = new Point(int.Parse(commands[1][1].ToString()), GetIndexForLetter(commands[1][0]));

            if (!manager.Move(from, to))
            {
                Console.WriteLine("Invalid move!");
                return;
            }
        }

        private int GetIndexForLetter(char letter)
        {
            for (int i = 0; i < GameBoard.Alphabet.Length; i++)
            {
                if (GameBoard.Alphabet[i] == letter)
                    return i + 1;
            }

            return -1;
        }

        private bool VerifyInput(string input, out string[] commands)
        {
            commands = input.Split(new char[] { ' ' });

            if (commands.Length != 2)
                return false;

            if (commands[0].Length != 2 || commands[1].Length != 2)
                return false;

            if (!GameBoard.Alphabet.Contains(commands[0][0]) || 
                !GameBoard.Alphabet.Contains(commands[1][0]))
                return false;

            if (!int.TryParse(commands[0][1].ToString(), out int from) ||
               !int.TryParse(commands[1][1].ToString(), out int to))
                return false;

            return true;
        }

        private bool GetYesNoInput(string prompt)
        {
            do
            {
                Console.Write(prompt + " (Y/N): ");
                string input = Console.ReadLine().ToUpper();

                if (input != "Y" && input != "N")
                    continue;
                else if (input == "Y")
                    return true;
                else
                    return false;
            }
            while (true);
        }

        private void Board_OnMoved(object sender, MoveEventArgs e)
        {
            if (GameOver)
                return;

            if (manager.VersusAI && e.PieceMoved.Owner == Player.Player2)
            {
                Console.WriteLine("\nPlayer 2: " + e.OldPosition.BoardPositionString() + ' ' +
                                                   e.NewPosition.BoardPositionString());
            }
            else if(!manager.VersusAI)
            {
                string message = string.Empty;

                if (e.PieceMoved.Owner == Player.Player1)
                    message = "Player 1: ";
                else
                    message = "Player 2: ";

                message += e.OldPosition.BoardPositionString() + ' ' + e.NewPosition.BoardPositionString();
                Console.WriteLine(message + '\n');
            }

            Draw();
        }

        private void Manager_OnGameOver(object sender, GameOverEventArgs e)
        {
            switch(e.Winner)
            {
                case Player.None:
                    Console.WriteLine("\nTie!");
                    break;
                case Player.Player1:
                    Console.WriteLine("\nPlayer 1 wins!");
                    break;
                case Player.Player2:
                    Console.WriteLine("\nPlayer 2 wins!");
                    break;
            }

            if (GetYesNoInput("\nPlay again?"))
                NewGame();
            else
                GameOver = true;
        }
    }
}
