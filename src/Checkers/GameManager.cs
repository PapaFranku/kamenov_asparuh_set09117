﻿using System;
using System.Collections.Generic;
using Checkers.Events;
using Checkers.AI;

namespace Checkers
{
    public class GameManager : ICloneable
    {
        public GameBoard Board { get; }

        public Player PlayerTurn { get; private set; }

        public event OnGameOverEventHandler OnGameOver;

        public bool VersusAI { get; }
        public bool IsGameOver { get; private set; }
        public bool ForceJumps { get; }
        public bool CanJumpAgain
        {
            get { return canJumpAgain; }
            set
            {
                if (ForceJumps)
                    throw new InvalidOperationException("Cannot cancel jump when " + nameof(ForceJumps) + " is set to true!");

                if (value is false && canJumpAgain is false)
                    throw new InvalidOperationException("The current player has no jump moves available!");

                canJumpAgain = value;

                NextTurn();
            }
        }

        internal Stack<MoveEventArgs> Moves { get; private set; }

        private Stack<GameManager> UndoStates;
        private Stack<GameManager> RedoStates;

        private bool canJumpAgain;

        private AIPlayer ai;

        public GameManager(GameBoard board, bool forceJumps, bool versusAI)
        {
            if (board is null)
                throw new ArgumentNullException("The board instance is null!");

            Board = board;
            Moves = new Stack<MoveEventArgs>();
            PlayerTurn = Player.Player1;
            IsGameOver = false;
            canJumpAgain = false;
            ForceJumps = forceJumps;
            VersusAI = versusAI;

            UndoStates = new Stack<GameManager>();
            RedoStates = new Stack<GameManager>();

            Board.OnMoved += Board_OnMoved;

            if (versusAI)
                ai = new AIPlayer(4);
        }

        //Used for copying the state for AI
        internal GameManager(GameBoard board, bool forceJumps) : this(board, forceJumps, false)
        {
            UndoStates = null;
            RedoStates = null;
        }

        public bool Move(Point from, Point to)
        {
            if (IsGameOver)
                return false;

            bool isJump = IsJumpMove(from, to);

            if (ForceJumps || canJumpAgain)
            {                                                          //If the player has jump moves
                if (!isJump && HasJumpMoves())                         //but is not issuing one,
                    return false;                                      //then it is not a valid move

                if (canJumpAgain && from != Moves.Peek().NewPosition)  //If the player is not jumping with the same piece
                    return false;                                      //then it is not a valid move either
            }

            GameManager newState = null;
            if (UndoStates != null)
                newState = this.Clone() as GameManager;

            if (!Board.Move(from, to, PlayerTurn, isJump))
                return false;

            if (UndoStates != null)
                UndoStates.Push(newState);

            if (RedoStates != null && RedoStates.Count > 0)
                RedoStates.Clear();

            if(IsGameOver)
                Manager_OnGameOver(new GameOverEventArgs(PlayerTurn));
            else if (VersusAI && PlayerTurn == Player.Player2)
                ai.NextMove(this);

            return true;
        }

        public bool Undo()
        {
            if (UndoStates.Count == 0)
                return false;

            if (VersusAI)
            {
                while (true)
                {
                    if (UndoStates.Count == 0)
                        break;

                    GameManager state = UndoStates.Pop();

                    if (UndoStates.Count == 0)
                    {
                        RedoStates.Push(this.Clone() as GameManager); //Copy the state before pushing
                        ReplaceCurrentState(state);
                    }
                    else if (state.PlayerTurn == Player.Player1)
                    {
                        RedoStates.Push(this.Clone() as GameManager); //Copy the state before pushing
                        ReplaceCurrentState(state);
                        break;
                    }
                }
            }
            else
            {
                GameManager state = UndoStates.Pop();
                RedoStates.Push(this.Clone() as GameManager);
                ReplaceCurrentState(state);
            }

            return true;
        }

        public bool Redo()
        {
            if (RedoStates.Count == 0)
                return false;

            if(VersusAI)
            {
                while(true)
                {
                    if (RedoStates.Count == 0)
                        break;

                    GameManager state = RedoStates.Pop();

                    if(state.PlayerTurn == Player.Player1)
                    {
                        UndoStates.Push(this.Clone() as GameManager); //Copy the state before pushing
                        ReplaceCurrentState(state);
                        break;
                    }
                }
            }
            else
            {
                GameManager state = RedoStates.Pop();
                UndoStates.Push(this.Clone() as GameManager); //Copy the state before pushing
                ReplaceCurrentState(state);
            }

            return true;
        }

        public Move[] ExportMoves()
        {
            if (Moves.Count == 0)
                return Array.Empty<Move>();

            Stack<MoveEventArgs> copy = new Stack<MoveEventArgs>(Moves); //The copy constuctor reverses the stack
            Move[] moves = new Move[copy.Count];

            int index = 0;

            while(copy.Count > 0)
            {
                MoveEventArgs args = copy.Pop(); 
                moves[index] = new Move(args.OldPosition, args.NewPosition);

                index++;
            }

            return moves;
        }

        public object Clone()
        {
            GameBoard newBoard = Board.Clone() as GameBoard;
            GameManager newManager = new GameManager(newBoard, ForceJumps, VersusAI)
            {
                IsGameOver = this.IsGameOver,
                canJumpAgain = this.canJumpAgain,
                PlayerTurn = this.PlayerTurn
            };

            MoveEventArgs[] moves = new MoveEventArgs[Moves.Count];
            int index = Moves.Count - 1;

            foreach(MoveEventArgs args in Moves)
            {
                moves[index] = args.Clone() as MoveEventArgs;
                index--;
            }

            newManager.Moves = new Stack<MoveEventArgs>(moves);

            return newManager;
        }

        internal GameManager CloneStateForAI()
        {
            GameBoard newBoard = Board.Clone() as GameBoard;
            GameManager newManager = new GameManager(newBoard, ForceJumps);
            newManager.canJumpAgain = canJumpAgain;
            newManager.PlayerTurn = PlayerTurn;

            MoveEventArgs[] moves = new MoveEventArgs[Moves.Count];
            int index = Moves.Count - 1;

            foreach (MoveEventArgs args in Moves)
            {
                moves[index] = args.Clone() as MoveEventArgs;
                index--;
            }

            newManager.Moves = new Stack<MoveEventArgs>(moves);

            return newManager;
        }

        internal bool IsJumpMove(Point oldPos, Point newPos)
        {
            if (oldPos.X - newPos.X == 1 ||
                oldPos.X - newPos.X == -1) //The piece has moved only 1 row ahead or behind
                return false;

            return true;
        }

        protected virtual void Manager_OnGameOver(GameOverEventArgs e)
        {
            if (OnGameOver != null)
                OnGameOver(this, e);
        }

        private void ReplaceCurrentState(GameManager state)
        {
            Board.board = state.Board.board;
            IsGameOver = state.IsGameOver;
            canJumpAgain = state.CanJumpAgain;
            Moves = state.Moves;
            PlayerTurn = state.PlayerTurn;
        }

        private bool HasJumpMoves()
        {
            foreach (Piece piece in Board.board)
            {
                if (PlayerTurn == piece.Owner && piece.IsInPlay)
                {
                    foreach (Point move in piece.AvailableMoves)
                    {
                        if (IsJumpMove(piece.Position, move))
                            return true;
                    }
                }
            }

            return false;
        }

        private void Board_OnMoved(object sender, MoveEventArgs e)
        {
            Moves.Push(e);

            CheckGameOver();

            if (IsGameOver)
                return;

            if (e.IsJumpMove)
            {
                foreach(Point move in e.PieceMoved.AvailableMoves) //Check if can jump again
                {
                    if(IsJumpMove(e.NewPosition, move))
                    {
                        canJumpAgain = true;
                        return; //If can jump again, don't change player turn
                    }
                }
            }

            canJumpAgain = false;
            NextTurn();
        }

        private void NextTurn()
        {
            if (IsGameOver)
                return;
            
            if (PlayerTurn == Player.Player1)
                PlayerTurn = Player.Player2;
            else
                PlayerTurn = Player.Player1;
        }

        private void CheckGameOver()
        {
            bool player1 = false;
            bool player2 = false;

            foreach (Piece piece in Board.board)
            {
                if (player1 && player2) //If both players have moves, stop checking
                    break;

                if (!piece.IsInPlay)
                    continue;

                foreach (Point move in piece.AvailableMoves) //Check if each player has available moves
                {
                    if (piece.Owner == Player.Player1)
                        player1 = true;
                    else
                        player2 = true;
                }

            }

            if (player1 && player2)
                return;

            Player winner = Player.None;

            if (player1 is true && player2 is false)
                winner = Player.Player1;

            if (player1 is false && player2 is true)
                winner = Player.Player2;

            IsGameOver = true;
            PlayerTurn = winner;
        }
    }
}
