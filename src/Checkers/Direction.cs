﻿namespace Checkers
{
    internal enum Direction
    {
        Left = 0,
        Right = 1,
        BackLeft = 2,
        BackRight = 3
    }
}
