﻿using System;
using System.Linq;
using Checkers.Events;

namespace Checkers.AI
{
    internal class AIPlayer
    {
        private int depth;

        internal AIPlayer(int depth)
        {
            this.depth = depth;
        }

        public void NextMove(GameManager manager)
        {
            int highscore = int.MinValue;

            Point from = new Point(1, 1);
            Point to = new Point(1, 1);

            GameManager newState = manager.CloneStateForAI();

            var pieces = manager.Board.board.Where(x => x.Owner == Player.Player2 && x.IsInPlay).ToList();
            foreach (Piece piece in pieces)
            {
                foreach (Point move in piece.AvailableMoves)
                {
                    if (!newState.Move(piece.Position, move))
                        continue;
                   
                    if(manager.CanJumpAgain)
                    {
                        from = piece.Position;
                        to = move;
                        break;
                    }

                    int newScore = Minimax(newState, Player.Player1, depth - 1); //It is the other player' turn

                    if(newScore > highscore)
                    {
                        highscore = newScore;
                        from = piece.Position;
                        to = move;
                    }

                    newState = manager.CloneStateForAI();
                }
            }

            manager.Move(from, to);
        }

        private int Minimax(GameManager state, Player player, int depth)
        {
            if (depth == 0)
                return Evaluate(state);

            if(player == Player.Player2)
            {
                int bestScore = int.MinValue + 1000;
                GameManager newState = state.CloneStateForAI();

                foreach (Piece piece in newState.Board.board.Where(x => x.Owner == Player.Player2 && x.IsInPlay))
                {
                    foreach(Point move in piece.AvailableMoves)
                    {
                        if (!newState.Move(piece.Position, move))
                            continue;
                     
                        int newScore = Minimax(newState, Player.Player1, depth - 1);
                        bestScore = Math.Max(bestScore, newScore);
                        newState = state.CloneStateForAI();
                    }
                }

                return bestScore;
            }
            else
            {
                int bestScore = int.MaxValue - 1000;
                GameManager newState = state.CloneStateForAI();

                foreach (Piece piece in newState.Board.board.Where(x => x.Owner == Player.Player1 && x.IsInPlay))
                {
                    foreach (Point move in piece.AvailableMoves)
                    {
                        if (!newState.Move(piece.Position, move))
                            continue;

                        int newScore = Minimax(newState, Player.Player2, depth - 1);
                        bestScore = Math.Min(bestScore, newScore);
                        newState = state.CloneStateForAI();
                    }
                }

                return bestScore;
            }
        }

        private int Evaluate(GameManager state)
        {
            int score = 0;
            
            MoveEventArgs lastMove = state.Moves.Peek();

            if (state.IsGameOver)
            {
                if (lastMove.PieceMoved.Owner == Player.Player1)
                    return int.MaxValue - 1000;
                else
                    return int.MinValue + 1000;
            }
            
            if (lastMove.PieceMoved.Owner == Player.Player2 && lastMove.IsJumpMove)
                score += 500;

            if (state.CanJumpAgain && state.PlayerTurn == Player.Player2)
                score += 2000;
            
            foreach (Piece piece in state.Board.board.Where(x => x.IsInPlay))
            {
                if (piece.Owner == Player.Player2)
                    score += 20;
                else
                    score -= 20;

                foreach (Point move in piece.AvailableMoves)
                {
                    if (piece.Owner == Player.Player2 && state.IsJumpMove(piece.Position, move))
                        score += 500;
                    else
                        score += 10;

                    if(piece.IsKing && lastMove.NewPosition != piece.Position)
                        score += 500;
                }
            }
            
            return score;
        }
    }
}
