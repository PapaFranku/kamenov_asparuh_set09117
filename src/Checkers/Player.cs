﻿namespace Checkers
{
    public enum Player
    {
        None = 0,   //Used when the game is a tie
        Player1 = 1,
        Player2 = 2
    }
}
