﻿using System;

namespace Checkers
{
    [Serializable]
    public struct Move
    {
        public Point From { get; }
        public Point To { get; }

        public Move(Point from, Point to)
        {
            From = from;
            To = to;
        }
    }
}
