﻿using System;
using System.Linq;
using Checkers.Events;

namespace Checkers
{
    public class GameBoard : ICloneable
    {
        public static readonly char[] Alphabet = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };
        public const int BoardSize = 8;
        public event OnMovedEventHandler OnMoved;

        internal Piece[] board;

        public GameBoard()
        {
            board = new Piece[24];
            InitializeBoard();
        }

        public Piece this[int x, int y]
        {
            get
            {
                Point position = new Point(x, y);

                return GetPieceAtPoint(position);
            }
        }

        public Piece this[Point position]
        {
            get
            {
                return GetPieceAtPoint(position);
            }
        }

        public object Clone()
        {
            Piece[] pieces = new Piece[24];

            for(int i = 0; i < board.Length; i++)
            {
                Piece newPiece = board[i].Clone() as Piece;

                pieces[i] = newPiece;
            }

            GameBoard newBoard = new GameBoard();
            newBoard.board = pieces;

            return newBoard;
        }

        internal bool Move(Point currentPos, Point newPos, Player playerTurn, bool isJump)
        {
            if (newPos.IsOutOfBounds()) // If the "to" parameter is invalid, no need to continue
                return false;

            Piece piece = this[currentPos];

            if (piece is null) //If "from" the parameter is invalid, no need to continue
                return false;

            if (!piece.AvailableMoves.Contains(newPos) || 
                piece.Owner != playerTurn)
                return false;

            piece.Position = newPos; //Move the piece
            piece.IsKing = IsKing(piece);

            if (isJump)
            {
                Piece jumpedPiece = GetJumpedPiece(currentPos, newPos);
                jumpedPiece.IsInPlay = false;   //Remove the jumped piece from play
                jumpedPiece.AvailableMoves = Array.Empty<Point>();
            }

            foreach (Piece p in board) //Update the pieces
            {
                if(p.IsInPlay)
                    p.AvailableMoves = GetAvailableMoves(p);
            }

            MoveEventArgs e = new MoveEventArgs(piece, isJump, currentPos);
            Board_OnMoved(e); //Send event that a piece has been moved

            return true;
        }

        protected virtual void Board_OnMoved(MoveEventArgs e)
        {
            if (OnMoved != null)
                OnMoved(this, e);
        }

        private Piece GetPieceAtPoint(Point position)
        {
            if (position.IsOutOfBounds())
                throw new ArgumentOutOfRangeException("Invalid bounds!");

            foreach (Piece piece in board)
            {
                if (piece.Position == position && piece.IsInPlay)
                    return piece;
            }

            return null;
        }

        private Piece GetJumpedPiece(Point oldPos, Point newPos)
        {
            int row;
            int col;

            if (oldPos.X > newPos.X) //Find which way the piece jumped
                row = oldPos.X - 1;
            else
                row = newPos.X - 1;

            if (oldPos.Y > newPos.Y) //Find which way the piece jumped
                col = oldPos.Y - 1;
            else
                col = newPos.Y - 1;

            return this[row, col];
        }

        private Point[] GetAvailableMoves(Piece piece)
        {
            Point[] RemoveNullEntries(Point[] arr)
            {
                return arr.Where(x => x != null).ToArray();
            }

            int size = piece.IsKing ? 4 : 2;

            Point[] moves = new Point[size];
            Point currentPos = piece.Position;

            //Left
            Point point = GetPointRelativeTo(currentPos, piece.Owner, Direction.Left);

            if (point != null)
                moves[0] = GetMoveAt(point, piece.Owner, Direction.Left);

            //Right
            point = GetPointRelativeTo(currentPos, piece.Owner, Direction.Right);

            if (point != null)
                moves[1] = GetMoveAt(point, piece.Owner, Direction.Right);

            if (!piece.IsKing)
                return RemoveNullEntries(moves);

            //Back Left
            point = GetPointRelativeTo(currentPos, piece.Owner, Direction.BackLeft);

            if (point != null)
                moves[2] = GetMoveAt(point, piece.Owner, Direction.BackLeft);

            //Back Right
            point = GetPointRelativeTo(currentPos, piece.Owner, Direction.BackRight);

            if (point != null)
                moves[3] = GetMoveAt(point, piece.Owner, Direction.BackRight);

            return RemoveNullEntries(moves);
        }

        private Point GetMoveAt(Point point, Player owner, Direction direction)
        {
            Piece pieceAtPos = this[point];

            if (pieceAtPos is null) //cell is free, can move there
                return point;

            if (pieceAtPos.Owner == owner) //The piece there is owned by the same player
                return null;               // No need to check if it can be jumped

            Point newPoint = GetPointRelativeTo(pieceAtPos.Position, owner, direction);

            if (newPoint is null) // point is otside bounds
                return null;

            Piece pieceAtNewPoint = this[newPoint];

            if (pieceAtNewPoint is null) //cell is free, can jump
                return newPoint;
            else
                return null; //cell is occupied, cannot jump
        }

        private Point GetPointRelativeTo(Point point, Player owner, Direction direction)
        {
            Point newPoint;

            switch (direction)
            {
                case Direction.Left:
                    if (owner == Player.Player1)
                        newPoint = new Point(point.X - 1, point.Y - 1);
                    else
                        newPoint = new Point(point.X + 1, point.Y + 1);

                    if (newPoint.IsOutOfBounds())
                        return null;
                    else
                        return newPoint;

                case Direction.Right:
                    if (owner == Player.Player1)
                        newPoint = new Point(point.X - 1, point.Y + 1);
                    else
                        newPoint = new Point(point.X + 1, point.Y - 1);

                    if (newPoint.IsOutOfBounds())
                        return null;
                    else
                        return newPoint;

                case Direction.BackLeft:
                    if (owner == Player.Player1)
                        newPoint = new Point(point.X + 1, point.Y - 1);
                    else
                        newPoint = new Point(point.X - 1, point.Y + 1);

                    if (newPoint.IsOutOfBounds())
                        return null;
                    else
                        return newPoint;

                case Direction.BackRight:
                    if (owner == Player.Player1)
                        newPoint = new Point(point.X + 1, point.Y + 1);
                    else
                        newPoint = new Point(point.X - 1, point.Y - 1);

                    if (newPoint.IsOutOfBounds())
                        return null;
                    else
                        return newPoint;
                default:
                    return null;
            }
        }

        private bool IsKing(Piece piece)
        {
            if (piece.IsKing)
                return true;

            if (piece.Owner == Player.Player1)
            {
                if (piece.Position.X == 1)
                    return true;
            }
            else
            {
                if (piece.Position.X == 8)
                    return true;
            }

            return false;
        }

        private void InitializeBoard()
        {
            int index = 0; //represents the index of the pieces array
            Player player = Player.Player2; //represents the player which the newly created piece will belong to

            for (int x = 0; x < BoardSize; x++)
            {
                if (x == 3) //If we are at the third row, then in the next iteration 
                {           //begin creating the pieces for the other player, which are 3 rows apart
                    x = 5;
                    player = Player.Player1;
                }

                bool isOddRow = x % 2 != 0;

                if (isOddRow)
                {
                    for (int y = BoardSize - 2; y >= 0; y--) //In this case the 8th cell of 
                    {                                        //the row will be empty
                        if (y % 2 != 0) //Pieces must be 1 cell apart
                            continue;

                        Piece piece = new Piece(x + 1, y + 1, player);
                        board[index] = piece;

                        index++;
                    }
                }
                else
                {
                    for (int y = BoardSize - 1; y >= 0; y--) //In this case the 8th cell of 
                    {                                        //the row will contain a piece
                        if (y % 2 == 0) //Pieces must be 1 cell apart
                            continue;

                        Piece piece = new Piece(x + 1, y + 1, player);
                        board[index] = piece;

                        index++;
                    }
                }
            }

            foreach(Piece piece in board) //Calculate the available moves for each piece
                piece.AvailableMoves = GetAvailableMoves(piece);
        }
    }
}
