﻿using System;

namespace Checkers
{
    [Serializable]
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point(Point other)
        {
            X = other.X;
            Y = other.Y;
        }

        public static bool operator ==(Point lhs, Point rhs)
        {
            if (lhs is null || rhs is null)
                return false;

            return lhs.X == rhs.X && lhs.Y == rhs.Y;
        }

        public static bool operator !=(Point lhs, Point rhs)
        {
            if (lhs is null && rhs is null)
                return false;

            if (lhs is null || rhs is null)
                return true;

            return lhs.X != rhs.X || lhs.Y != rhs.Y;
        }

        public override bool Equals(object obj)
        {
            if (obj is Point == false)
                return false;

            Point point = obj as Point;

            return this == point;
        }

        public override int GetHashCode()
        {
            return (X + Y).GetHashCode();
        }
    }

    public static class PointExtensions
    {
        public static string BoardPositionString(this Point point)
        {
            return string.Concat(GameBoard.Alphabet[point.Y - 1].ToString() + point.X);
        }

        public static bool IsOutOfBounds(this Point point)
        {
            if (point.X < 1 || point.X > 8 ||
                point.Y < 1 || point.Y > 8)
                return true;

            return false;
        }
    }
}
