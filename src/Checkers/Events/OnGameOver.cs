﻿using System;

namespace Checkers.Events
{
    public delegate void OnGameOverEventHandler(object sender, GameOverEventArgs e);

    public class GameOverEventArgs : EventArgs
    {
        public Player Winner { get; }

        internal GameOverEventArgs(Player winner)
        {
            Winner = winner;
        }
    }
}
