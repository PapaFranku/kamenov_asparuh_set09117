﻿using System;

namespace Checkers.Events
{
    public delegate void OnMovedEventHandler(object sender, MoveEventArgs e);

    public class MoveEventArgs : EventArgs, ICloneable
    {
        public Piece PieceMoved { get; }
        public bool IsJumpMove { get; }
        public Point OldPosition { get; }
        public Point NewPosition { get; private set; }

        internal MoveEventArgs(Piece pieceMoved, bool isJump, Point oldPos)
        {
            PieceMoved = pieceMoved;
            IsJumpMove = isJump;
            OldPosition = new Point(oldPos);
            NewPosition = new Point(pieceMoved.Position);
        }

        public object Clone()
        {
            Piece piece = PieceMoved.Clone() as Piece;

            MoveEventArgs args = new MoveEventArgs(piece, IsJumpMove, new Point(OldPosition));
            args.NewPosition = new Point(NewPosition);

            return args;
        }
    }
}
