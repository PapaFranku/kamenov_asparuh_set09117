﻿using System;

namespace Checkers
{
    public class Piece : ICloneable
    {
        public bool IsKing { get; internal set; }
        public bool IsInPlay { get; internal set; }
        public Point Position { get; internal set; }
        public Point[] AvailableMoves { get; internal set; }
        public Player Owner { get; }

        public Piece(Point position, Player owner)
        {
            Position = position;
            Owner = owner;
        }

        public Piece(int x, int y, Player owner)
        {
            Position = new Point(x, y);
            Owner = owner;
            IsInPlay = true;
        }

        public object Clone()
        {
            Piece piece = new Piece(Position.X, Position.Y, Owner);
            piece.IsInPlay = IsInPlay;
            piece.IsKing = IsKing;
            piece.AvailableMoves = new Point[AvailableMoves.Length];

            for(int i = 0; i < AvailableMoves.Length; i++)
            {
                piece.AvailableMoves[i] = new Point(AvailableMoves[i].X, AvailableMoves[i].Y);
            }

            return piece;
        }
    }
}
