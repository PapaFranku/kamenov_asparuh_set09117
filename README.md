# Algorithms & Data Structures (SET09117)

Contains the files required for the Checkers game coursework for **SET09117** module at Napier University.

### Build in Visual Studio:
1. Build the **"Checkers"** project **first** (right click on the project -> Build)
2. Build the **"ConsoleClient"** and **"GUIClient"** projects in any order